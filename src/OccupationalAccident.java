public class OccupationalAccident implements Comparable<OccupationalAccident> {
    private String sector;
    private int noPD, noNPD, noDeath;

    //normal constructor
    public OccupationalAccident(String s, int npd, int nnpd, int nd)  {
        sector = s;
        noPD = npd;
        noNPD = nnpd;
        noDeath = nd;
    }

    //mutator
    public void setNoPD(int n) {
        noPD = n;
    }

    public void setNoNPD(int n) {
        noNPD = n;
    }

    public void setNoDeath(int n) {
        noDeath = n;
    }

    //retriever
    public String getSector() {
        return sector;
    }

    public int getNoPD() {
        return noPD;
    }

    public int getNoNPD() {
        return noNPD;
    }

    public int getNoDeath() {
        return noDeath;
    }

    public int calcTotalCase() {
        return getNoPD() + getNoNPD() + getNoDeath();
    }

    //printer
    public String toString() {
        return getSector() + ": \t" + getNoPD() + "\t" + getNoNPD() + "\t" + getNoDeath();
    }

    @Override
    public int compareTo(OccupationalAccident oa)
    {
        int compare = oa.getNoDeath()+oa.getNoNPD()+oa.getNoPD();
        return this.getNoPD()+this.getNoNPD()+this.getNoDeath()-compare;
    }
}