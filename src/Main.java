import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        //Question 1
        List<OccupationalAccident> accidentList = new ArrayList<OccupationalAccident>();

        //Question 2
        FileReader fr;
        try {
            fr = new FileReader("C:\\apikk\\project\\test\\CSC248As1\\src\\accidentCases.txt");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return;
        }
        BufferedReader br = new BufferedReader(fr);

        //Question 3
        PrintWriter pw;
        try {
            pw = new PrintWriter(new File("accidentReport.txt"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Question 4
        try {
            while (br.ready()) {
                StringTokenizer st = new StringTokenizer(br.readLine(), ";");
                OccupationalAccident oa = new OccupationalAccident(st.nextToken(), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
                accidentList.add(oa);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        //Question 5
        for (var oa : accidentList) {
            if (oa.getSector().equals("Pembinaan"))
                oa.setNoDeath(550);
        }

        // Question 6
        // gue lupa bbsort so i guna my own way
        Collections.sort(accidentList);
        Collections.reverse(accidentList);

        //Question 7
        for (var oa : accidentList
        ) {
            pw.println(String.format("%s: %d %d %d %d",oa.getSector(),oa.getNoPD(),oa.getNoNPD(),oa.getNoDeath(),(oa.getNoPD()+oa.getNoNPD()+oa.getNoDeath())));
        }
        pw.close();

        //Question 8
        //susun sendiri
        System.out.println("Occupational Accident Statistics by Sector Until September 2022 in descending order.");
        System.out.println("SECTOR        PD        NPD      D     TOTAL");
        for (var oa : accidentList
        ) {
            System.out.println(String.format("%s: %d %d %d %d",oa.getSector(),oa.getNoPD(),oa.getNoNPD(),oa.getNoDeath(),(oa.getNoPD()+oa.getNoNPD()+oa.getNoDeath())));
        }
    }
}